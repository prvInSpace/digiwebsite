
var text;

function s(key){
	if(key in text){
		if(lang in text[key])
			return text[key][lang];
		else
			return text[key]['en'];
	}
	return key;
}

async function loadTranslations(){
	var res = await fetch("translations.json")
	var obj = await res.json()
	text = obj;
	console.log("Info: Translations loaded");
}

//loadTranslations();
