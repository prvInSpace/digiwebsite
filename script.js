"use strict";

var api = "https://api.prv.cymru";
var queue = [];

// Fetches the string for the given key
window.onload = async function(){
	changeColor();
	await loadTranslations();
	console.log("Info: Running functions");
	queue.forEach(s => s())
}


function keyPress(enter, callback) {
	enter = enter || window.event;
	if (enter.keyCode == 13) {
		if(callback != null)
			callback();
		return false;
	}
	return true;
}

function searchById(lang, id){
	var params = {};
	params.dict = lang;
	params.id = id;
	setPage(params);
}

function createClickableWord(word){
	var button = document.createElement('button');
	button.classList.add('clickableWord');
	button.onclick = function(){searchById(word.lang, word.id)};
	button.innerHTML = word.value;
	return button;
}

function fetchAndHandleJsonRequest(url, callback){
	console.log("Fetching: " + url);
	fetch(url)
		.then(res => res.json())
		.then((out) => {
			handleResponse(out, callback)
		})
		.catch(err => {
			// Incase we fail to contact or convert JSON from API, give the user an error message.
			var results = createResultsDiv();
			var div = document.createElement("div")
			div.classList.add('result');
			div.appendChild(createTextElement('h2', 'No connection to API :('));
			div.appendChild(createTextElement('h3', 'Reason: ' + err.message));
			results.appendChild(div);
			var body = document.getElementsByTagName("body")[0];
			var oldResult = document.getElementById("results");
			body.replaceChild(results, oldResult);
		});
}

function createResultsDiv(){
	var results = document.createElement("div")
	results.id = "results";
	results.classList.add("tab");
	results.classList.add("results");
	return results;
}

function replaceResults(results){
	var body = document.getElementsByTagName("body")[0];
	var oldResult = document.getElementById("results");
	body.replaceChild(results, oldResult);
}

function handleResponse(obj, callback) {
	var results = document.createElement("div")
	results.id = "results";
	results.classList.add("tab");
	results.classList.add("results");
	if(obj.success != true){
		var div = document.createElement("div")
		div.classList.add('result');
		div.appendChild(createTextElement('h2', 'Failed to get data from API :('));
		div.appendChild(createTextElement('h3', 'Reason: ' + obj.error));
		results.appendChild(div);
	}
	else
		callback(results, obj)

	var body = document.getElementsByTagName("body")[0];
	var oldResult = document.getElementById("results");
	body.replaceChild(results, oldResult);
}

function createRow(name, value){
	var tr = document.createElement('tr');
	tr.appendChild(createTextElement('th', name));
	if(Array.isArray(value)){
		var td = document.createElement('td');
		for (var i = 0; i < value.length; i++){
			if(i != 0)
				td.appendChild(document.createElement('br'));
			td.appendChild(value[i]);
		}
		tr.appendChild(td);
	}
	else if(value instanceof HTMLElement){
		var td = document.createElement('td');
		td.appendChild(value);
		tr.appendChild(td);
	}
	else {
		tr.appendChild(createTextElement('td', value))
	}
	return tr;
}


function createTextElement(type, text){
	var obj = document.createElement(type);
	obj.innerHTML = text;
	return obj;
}

function capitalize(string){
	if(string.lenght < 1)
		return string;
	var s = string.charAt(0).toUpperCase()
	if(string.length > 1)
		s += string.slice(1);
	return s;
}

/*
 * Reload the page with any new get variables
 */
function reloadPage(variables){
	var url = window.location.href.split('?');
	window.location.href = url[0] + '?' + variables;
}

function deleteCookies(){
	var cookies = document.cookie.split(";");

	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		var eqPos = cookie.indexOf("=");
		var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
		document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	}
	window.location.href = window.location.href.split('?')[0];
}

const colors = {
	"gd": "#0065BF",
	"br": "#4d5b6a",
	"cy": "#dd0000",
	"kw": "#f2a214",
	"es": "#fce205",
	"de": "#dd0000",
	"ga": "#009A44"
};

function changeColor(){
	var clrLang = lang;
	var element = document.getElementById("lang");
	if(element !== null)
		clrLang = element.value;
	if(clrLang in colors){
		document.documentElement.style.setProperty('--main-header-color', colors[clrLang]);
	}
}

function setPage(params){
	var url = new URL(window.location.href.split('?')[0])
	if(lang !== "en")
		params.lang = lang;

	for(var key in params)
		url.searchParams.set(key, params[key]);
	window.location.href = url.toString();
}
