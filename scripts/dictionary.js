'use strict';

function wordSearch() {

	var params = {};
	params.dict = document.getElementById("lang").value;
	params.word = document.getElementById("search").value.toLowerCase().trim();
	params.type = document.getElementById("type").value;
	params.search = document.getElementById('selectSearch').value;

	// Don't change page if word is empty
	if(params.word == "")
		return;

	setPage(params);
}

function fetchById(lang, id){
	fetchAndHandleJsonRequest(api
		+ '/api/dictionary?lang='
		+ lang
		+ '&id=' + id, 
		wordListToDiv);
}

function fetchWord(lang, word, type, search){
	fetchAndHandleJsonRequest(api + '/api/dictionary'
		+ "?lang=" + lang
		+ "&word=" + word
		+ (type != "" ? "&type=" + type : "")
		+ "&search=" + search,
		wordListToDiv);
}


