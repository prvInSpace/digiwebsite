
const fields = [
	"singFirst",
	"singSecond",
	"singThird",
	"plurFirst",
	"plurSecond",
	"plurThird",
	"impersonal"
];


function generateTense(){
	var words = document.getElementById('tenseText').value.match(/[^ ,]+/g);
	var code = document.createElement('textarea');
	code.readOnly = true;

	for(var i = 0; i < words.length && i < fields.length; ++i){
		if(i != 0)
			code.innerHTML += ',\n';
		code.innerHTML += '"' + fields[i] + '": ' +
			(words[i] == "-" ? "[]" : '"' + words[i] + '"');
	}
	code.innerHTML += '\n';
	code.rows = code.innerHTML.split('\n').length + 5;


	var results = createResultsDiv();
	results.append(code);
	replaceResults(results);
}

