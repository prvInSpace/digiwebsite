
<div id="numberSettings" class="tab" >
	<input type="number" id="numberSearch" placeholder="<?=s('placeholder_type_number')?>" onkeypress="return keyPress(event, numberSearch)"/>
	<input type="button" class="go" value="<?=s('submit')?>" onclick="numberSearch()">
</div>
<div id="results" class="results"></div>		
<script>
<?php
if(isset($_GET['number']))	
	echo "queue.push(() => fetchNumber({$_GET['number']}));\n";
?>
</script>

