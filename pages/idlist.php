<div id="idlist" class="tab">
	<select id="lang">
		<option value="br">Breton</option>
		<option value="kw">Cornish</option>
		<option value="de">German</option>
		<option value="ga">Irish</option>
		<option value="gd">Scottish Gaelic</option>
		<option value="cy">Welsh</option>
	</select>
	<label for="min">Minimum:</label>
	<input type="number" id="min" name="min" min="0" max="2000000">
	<label for="max">Maximum:</label>
	<input type="number" id="max" name="max" min="1" max="2000000">
	<input type="button" value="Submit" onclick="generateList()">
</div>
<div id="results" class="tab results"></div>		
