# Open Celtic Dictionary Website

The source code for the Open Celtic Dictionary web-interface website.

## Translations

This website uses a JSON file with all the strings and text used on the website.
This allows the website to be translated into several languages.
These translations are not complete, but if you want to contribute please feel free to do so :)

## Original Creator

As this repository is a fork of an original repository by Håvard, we want to pay homage to his original repository by leaving this image of Preben carrying Håvard through the creation of this website.

![Eyy](./img/carry.png)

